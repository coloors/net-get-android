package com.coloors.netget;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CreateAd extends AppCompatActivity {

    private CheckBox cb_type_0;
    private CheckBox cb_type_1;
    private CheckBox cb_type_2;

    private ImageButton image_add_0;
    private ImageButton image_add_1;
    private ImageButton image_add_2;
    private ImageButton image_add_3;
    private ImageButton image_add_4;

    private CheckBox cb_conditions;

    private static int SELECT_PICTURE = 0;
    private int selection_picture = 0;

    private static int VIEW_AD = 0;
    private static int VIEW_CUPON = 1;

    private int currentView = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final CreateAd me = this;

        setContentView(R.layout.activity_create_ad);
        Toolbar toolbar = (Toolbar) findViewById(R.id.create_ad_toolbar);
        //toolbar.setTitle("Nueva publicación");

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        ImageButton save_ad = (ImageButton) findViewById(R.id.save_ad);
        save_ad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!cb_conditions.isChecked()){
                    CharSequence text = "Debe aceptar las condiciones para poder publicar un aviso";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(me, text, duration);
                    toast.show();
                }
            }
        });

        onChangeView();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == SELECT_PICTURE && resultCode == RESULT_OK)
        {
            Uri uri = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);

                switch(selection_picture){
                    case 0:
                        image_add_0.setImageBitmap(bitmap);
                        break;
                    case 1:
                        image_add_1.setImageBitmap(bitmap);
                        break;
                    case 2:
                        image_add_2.setImageBitmap(bitmap);
                        break;
                    case 3:
                        image_add_3.setImageBitmap(bitmap);
                        break;
                    case 4:
                        image_add_4.setImageBitmap(bitmap);
                        break;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void deselectAllCB()
    {
        cb_type_0.setChecked(false);
        cb_type_1.setChecked(false);
        cb_type_2.setChecked(false);
    }

    public void onChangeView()
    {
        image_add_0 = (ImageButton) findViewById(R.id.image_add_0);
        image_add_0.requestFocus();
        image_add_0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                selection_picture =0;
                startActivityForResult(intent, SELECT_PICTURE);
            }
        });

        image_add_1 = (ImageButton) findViewById(R.id.image_add_1);
        image_add_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                selection_picture =1;
                startActivityForResult(intent, SELECT_PICTURE);
            }
        });

        image_add_2 = (ImageButton) findViewById(R.id.image_add_2);
        image_add_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                selection_picture =2;
                startActivityForResult(intent, SELECT_PICTURE);
            }
        });

        image_add_3 = (ImageButton) findViewById(R.id.image_add_3);
        image_add_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                selection_picture =3;
                startActivityForResult(intent, SELECT_PICTURE);
            }
        });

        image_add_4 = (ImageButton) findViewById(R.id.image_add_4);
        image_add_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                selection_picture =4;
                startActivityForResult(intent, SELECT_PICTURE);
            }
        });

        cb_type_0 = (CheckBox) findViewById(R.id.checkbox_type_0);
        cb_type_1 = (CheckBox) findViewById(R.id.checkbox_type_1);
        cb_type_2 = (CheckBox) findViewById(R.id.checkbox_type_2);
        cb_conditions = (CheckBox) findViewById(R.id.checkbox_type_9);

        cb_type_0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deselectAllCB();
                changeViewToCreateAd();
                currentView = VIEW_AD;
                cb_type_0.setChecked(true);
            }
        });

        cb_type_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deselectAllCB();
                changeViewToCreateAd();
                currentView = VIEW_AD;
                cb_type_1.setChecked(true);
            }
        });

        cb_type_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deselectAllCB();
                changeViewToCreateCupon();
                currentView = VIEW_CUPON;
                cb_type_2.setChecked(true);
            }
        });


        Spinner spin_cat = (Spinner) findViewById(R.id.spin_cat);
        List<String> categories = new ArrayList<String>();
        MyApp myApp = (MyApp)getApplicationContext();
        List<Categories> cats = myApp.getCategories();
        for(int i=0;i<cats.size();i++) categories.add(cats.get(i).getName());
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin_cat.setAdapter(dataAdapter);




    }
    public void changeViewToCreateAd()
    {
        if(currentView == VIEW_AD) return;
        View C = findViewById(R.id.create_ad_cupon);
        ViewGroup parent = (ViewGroup) C.getParent();
        int index = parent.indexOfChild(C);
        parent.removeView(C);
        C = getLayoutInflater().inflate(R.layout.content_create_ad, parent, false);
        parent.addView(C, index);
        onChangeView();
    }

    public void changeViewToCreateCupon()
    {
        if(currentView == VIEW_CUPON) return;
        View C = findViewById(R.id.create_ad_layout);
        ViewGroup parent = (ViewGroup) C.getParent();
        int index = parent.indexOfChild(C);
        parent.removeView(C);
        C = getLayoutInflater().inflate(R.layout.content_create_cupon, parent, false);
        parent.addView(C, index);
        onChangeView();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        LinearLayout images_layout = (LinearLayout) findViewById(R.id.images_layout);
        ImageButton imb1 = (ImageButton) findViewById(R.id.image_add_0);

        ViewGroup.LayoutParams lfm = images_layout.getLayoutParams();
        int height = imb1.getMeasuredWidth();


        lfm.width = ViewGroup.LayoutParams.MATCH_PARENT;
        lfm.height = height;

        images_layout.setLayoutParams(lfm);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
