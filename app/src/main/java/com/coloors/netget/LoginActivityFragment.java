package com.coloors.netget;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

/**
 * A placeholder fragment containing a simple view.
 */
public class LoginActivityFragment extends Fragment {

    private  CallbackManager callbackManager;


    public LoginActivityFragment() {
    }



    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        Button guessButton = (Button) view.findViewById(R.id.guess_button);
        guessButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginActivity login = (LoginActivity) getActivity();
                startActivity(new Intent(login,MainActivity.class));
            }
        });

        Button registerButton = (Button) view.findViewById(R.id.register_button);
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginActivity login = (LoginActivity) getActivity();
                startActivity(new Intent(login,RegisterActivity.class));
            }
        });

        Button alreadyButton = (Button) view.findViewById(R.id.already_button);
        alreadyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginActivity login = (LoginActivity) getActivity();
                startActivity(new Intent(login,MainActivity.class));
            }
        });

        LoginButton loginButton = (LoginButton) view.findViewById(R.id.login_button);
        loginButton.setReadPermissions("user_friends");
        // If using in a fragment
        loginButton.setFragment(this);
        // Other app specific specialization

        callbackManager = CallbackManager.Factory.create();


        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                LoginActivity login = (LoginActivity) getActivity();
                startActivity(new Intent(login,MainActivity.class));
                //Toast toast = Toast.makeText(getContext(), "success", Toast.LENGTH_SHORT);
                //toast.show();
            }

            @Override
            public void onCancel() {
                // App code
                Toast toast = Toast.makeText(getContext(), "cancel", Toast.LENGTH_SHORT);
                toast.show();
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Toast toast = Toast.makeText(getContext(), "error", Toast.LENGTH_SHORT);
                toast.show();
            }
        });

        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

}
