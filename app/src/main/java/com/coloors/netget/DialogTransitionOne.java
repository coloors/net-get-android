package com.coloors.netget;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by ngpeltzer on 2/19/16.
 */
 public class DialogTransitionOne extends DialogFragment {

    private int text_yes;
    private int image;
    private View.OnClickListener yes_callback;
    private int text;




    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        this.text_yes = args.getInt("text_yes");
        this.image = args.getInt("image");
        this.text = args.getInt("text");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog d = getDialog();
        if (d!=null){
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            d.getWindow().setLayout(width, height);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.dialog_transition_one_button, container, false);

        ImageButton close_button = (ImageButton) root.findViewById(R.id.transition_one_close_button);
        final DialogTransitionOne me = this;
        close_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                me.dismiss();
            }
        });

        TextView textView = (TextView) root.findViewById(R.id.transition_one_text);
        textView.setText(text);
        ImageView imageView = (ImageView) root.findViewById(R.id.transition_one_image);
        imageView.setImageResource(this.image);
        Button yes = (Button) root.findViewById(R.id.transition_one_yes);
        yes.setOnClickListener(yes_callback);
        yes.setText(text_yes);


        return root;
    }

    public void setYesCallback(View.OnClickListener callback)
    {
        this.yes_callback = callback;
    }
}