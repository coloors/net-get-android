package com.coloors.netget;

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.DisplayMetrics;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ViewFlipper;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.appevents.AppEventsLogger;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;



public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private ImageView userpicture;
    private ImageView coverpicture;
    private Menu menu;
    private List<Categories> categories;


    private HelpExpandableListAdapter helpExpandableListAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);



        setContentView(R.layout.activity_main);


        LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;
        List<String> list = lm.getAllProviders();

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {}

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {}

        if(!gps_enabled) {
            FragmentTransaction ft = getFragmentManager().beginTransaction();


            View.OnClickListener callback = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //startActivity(new Intent(this,MainActivity.class));
                }
            };


            Bundle args = new Bundle();
            args.putInt("text_no", R.string.not_now);
            args.putInt("text_yes", R.string.activate);
            args.putInt("image", R.drawable.location_icon);
            args.putInt("text", R.string.gps_network_not_enabled);
            DialogTransitionTwo frag = new DialogTransitionTwo();
            frag.setArguments(args);
            frag.setYesCallback(callback);
            frag.setNoCallback(callback);
            frag.show(ft, new String("txn_tag"));
        }

        final MainActivity me = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ColorDrawable white = new ColorDrawable(0xFFFFFFFF);
        //toolbar.setBackground(white);
        toolbar.setTitleTextColor(Color.BLACK);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
                startActivity(new Intent(me,CreateAd.class));

            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ColorDrawable cyan = new ColorDrawable(0xFF36c4ee);

        //drawer.setBackground(cd);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        MenuItem newMenuItem = navigationView.getMenu().findItem(R.id.nav_new);
        onNavigationItemSelected(newMenuItem);

        navigationView.setBackground(cyan);
        int[][] states = new int[][] {
                new int[] { android.R.attr.state_enabled}, // enabled
                new int[] {-android.R.attr.state_enabled}, // disabled
                new int[] {-android.R.attr.state_checked}, // unchecked
                new int[] { android.R.attr.state_pressed}  // pressed
        };

        int[] colors = new int[] {
                Color.WHITE,
                Color.GRAY,
                Color.WHITE,
                Color.WHITE
        };

        ColorStateList myList = new ColorStateList(states, colors);
        navigationView.setItemTextColor(myList);
        navigationView.setItemIconTintList(myList);
        navigationView.setNavigationItemSelectedListener(this);


        userpicture = (CircleImageView) navigationView.getHeaderView(0).findViewById(R.id.profile_image);
        coverpicture = (ImageView) navigationView.getHeaderView(0).findViewById(R.id.cover_image);

        Bundle params = new Bundle();
        params.putString("fields", "id,email,gender,cover,picture.type(large)");
        new GraphRequest(AccessToken.getCurrentAccessToken(), "me", params, HttpMethod.GET,
                new GraphRequest.Callback() {
                    @Override
                    public void onCompleted(GraphResponse response) {
                        if (response != null) {
                            try {
                                JSONObject data = response.getJSONObject();
                                if (data.has("picture")) {
                                    String profilePicUrl = data.getJSONObject("picture").getJSONObject("data").getString("url");
                                    List<ImageView> list = new ArrayList<ImageView>();
                                    list.add(userpicture);
                                    list.add(coverpicture);
                                    DownloadImagesTask dit = new DownloadImagesTask(list,profilePicUrl);
                                    dit.execute();


                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }).executeAsync();



    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);



        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        ViewFlipper vf = (ViewFlipper) findViewById(R.id.vf);
        if (id == R.id.nav_categories) {
            vf.setDisplayedChild(1);
            FrameLayout ll1 = (FrameLayout) findViewById(R.id.fm1);
            FrameLayout ll2 = (FrameLayout) findViewById(R.id.fm2);
            FrameLayout ll3 = (FrameLayout) findViewById(R.id.fm3);

            ViewGroup.LayoutParams lfm = ll1.getLayoutParams();
            int width = ll1.getMeasuredWidth();

            lfm.width = width;
            lfm.height = width;

            ll1.setLayoutParams(lfm);
            ll2.setLayoutParams(lfm);
            ll3.setLayoutParams(lfm);


        } else if (id == R.id.nav_contact) {

        } else if (id == R.id.nav_help) {
            vf.setDisplayedChild(2);

            ExpandableListView explist = (ExpandableListView) findViewById(R.id.helpExpandableListView);

            List<String> listDataHeader = new ArrayList<String>();
            HashMap<String, List<String>> listDataChild = new HashMap<String, List<String>>();
            // preparing list data
            prepareListData(listDataHeader,listDataChild);
            helpExpandableListAdapter = new HelpExpandableListAdapter(this, listDataHeader, listDataChild);
            // setting list adapter
            explist.setAdapter(helpExpandableListAdapter);

            DisplayMetrics metrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(metrics);
            int width = metrics.widthPixels;
            width = (int) (width * 0.90);
            if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
                explist.setIndicatorBounds(width-GetPixelFromDips(35), width-GetPixelFromDips(5));
            } else {
                explist.setIndicatorBoundsRelative(width-GetPixelFromDips(35), width-GetPixelFromDips(5));
            }



        } else if(id== R.id.nav_messages)
        {

            vf.setDisplayedChild(0);

            // Construct the data source
            ArrayList<MyMessage> arrayOfUsers = new ArrayList<MyMessage>();
            // Create the adapter to convert the array to views
            MessagesListAdapter adapter = new MessagesListAdapter(this, arrayOfUsers);

            MyMessage newMessage = new MyMessage("Nombre del usuario", "Nombre del producto");
            adapter.add(newMessage);
            adapter.add(newMessage);



            // Attach the adapter to a ListView
            ListView listView = (ListView) findViewById(R.id.messages_list);
            listView.setAdapter(adapter);

        } else if(id== R.id.nav_new)
        {
            vf.setDisplayedChild(3);

            // Construct the data source
            ArrayList<MyProduct> arrayOfProducts = new ArrayList<MyProduct>();
            // Create the adapter to convert the array to views
            ProductsListAdapter adapter = new ProductsListAdapter(this, arrayOfProducts);


            /*MyProduct newProduct = new MyProduct("Titulo del producto", "Descripción del producto");
            adapter.add(newProduct);
            adapter.add(newProduct);
            adapter.add(newProduct);
            adapter.add(newProduct);*/

            ProductsLoader pl = new ProductsLoader();
            pl.new SearchProducts(adapter).execute("url");

            // Attach the adapter to a ListView
            GridView gridView = (GridView) findViewById(R.id.products_grid);
            gridView.setAdapter(adapter);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }



    public int GetPixelFromDips(float pixels)
    {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }

    private void prepareListData(List<String> listDataHeader, HashMap<String, List<String>> listDataChild) {

        // Adding child data
        listDataHeader.add("¿Pregunta #1?");
        listDataHeader.add("¿Pregunta #2?");
        listDataHeader.add("¿Pregunta #3?");

        // Adding child data
        List<String> question1 = new ArrayList<String>();
        question1.add("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vel condimentum erat. Curabitur finibus convallis sem vitae varius. Morbi eleifend finibus placerat. Suspendisse tempus urna sit amet nibh luctus, ut pellentesque dolor cursus. In bibendum elementum lorem, quis maximus dui lacinia vel. Suspendisse molestie purus nisl, eget ultricies sapien ullamcorper et. Duis gravida tempor bibendum. Integer finibus varius ex, eu sagittis erat volutpat et.");

        List<String> question2 = new ArrayList<String>();
        question2.add("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vel condimentum erat. Curabitur finibus convallis sem vitae varius. Morbi eleifend finibus placerat. Suspendisse tempus urna sit amet nibh luctus, ut pellentesque dolor cursus. In bibendum elementum lorem, quis maximus dui lacinia vel. Suspendisse molestie purus nisl, eget ultricies sapien ullamcorper et. Duis gravida tempor bibendum. Integer finibus varius ex, eu sagittis erat volutpat et.");


        List<String> question3 = new ArrayList<String>();
        question3.add("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vel condimentum erat. Curabitur finibus convallis sem vitae varius. Morbi eleifend finibus placerat. Suspendisse tempus urna sit amet nibh luctus, ut pellentesque dolor cursus. In bibendum elementum lorem, quis maximus dui lacinia vel. Suspendisse molestie purus nisl, eget ultricies sapien ullamcorper et. Duis gravida tempor bibendum. Integer finibus varius ex, eu sagittis erat volutpat et.");


        listDataChild.put(listDataHeader.get(0), question1); // Header, Child data
        listDataChild.put(listDataHeader.get(1), question2);
        listDataChild.put(listDataHeader.get(2), question3);
    }



}
