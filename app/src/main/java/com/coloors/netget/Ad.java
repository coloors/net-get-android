package com.coloors.netget;

import android.graphics.Bitmap;

import java.util.Date;

/**
 * Created by ngpeltzer on 3/6/16.
 */
public class Ad {
    public String categorie;
    public String user_id;
    public String title;
    public String description;
    public Date datetime;
    public String transaction;
    public int duration;
    public Bitmap photo1;
    public Bitmap photo2;
    public Bitmap photo3;
    public Bitmap photo4;
    public Bitmap photo5;
    public String province;
    public String city;
    public float latitude;
    public float longitude;
    public boolean delivery;
    public boolean negotiable;
    public boolean exchange;
    public float price;

}
