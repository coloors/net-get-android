package com.coloors.netget;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        final RegisterActivity me = this;
        Button cancel = (Button) findViewById(R.id.register_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(me,LoginActivity.class));
            }
        });

        Button create = (Button) findViewById(R.id.register_create);
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();


                View.OnClickListener callback = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(me,MainActivity.class));
                    }
                };


                Bundle args = new Bundle();
                args.putInt("text_yes", R.string.start);
                args.putInt("image", R.drawable.dialog_check);
                args.putInt("text",R.string.create_account_succes);
                DialogTransitionOne frag = new DialogTransitionOne();
                frag.setArguments(args);
                frag.setYesCallback(callback);
                frag.show(ft, new String("txn_tag"));
            }
        });
    }



}
