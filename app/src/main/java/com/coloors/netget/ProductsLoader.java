package com.coloors.netget;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by ngpeltzer on 2/12/16.
 */
public final class ProductsLoader {

    public final static String apiURL = "http://netandget.com/API/api.php";


    public class SearchProducts extends AsyncTask<String, String, String> {

        private ProductsListAdapter adapter;
        public SearchProducts(ProductsListAdapter adapter){
            this.adapter = adapter;
        }

        @Override
        protected String doInBackground(String... params) {

            String urlString=params[0]; // URL to call

            InputStream is = null;
            String result = "";
            JSONObject jsonObject = null;

            // HTTP
            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(apiURL);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("get_posts", ""));
                nameValuePairs.add(new BasicNameValuePair("category", ""));
                nameValuePairs.add(new BasicNameValuePair("latitude", "40.415557"));
                nameValuePairs.add(new BasicNameValuePair("longitude", "-3.7092424"));
                nameValuePairs.add(new BasicNameValuePair("page", "1"));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));


                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
            } catch(Exception e) {
                return null;
            }

            // Read response to string
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is,"utf-8"),8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();
                Log.d("JSONRESPONSE",result);

            } catch(Exception e) {
                return null;
            }



            return result;
        }


        protected void onPostExecute(String result) {
            JSONObject jsonObj = null;
            JSONArray posts = null;
            try {
                jsonObj = new JSONObject(result);
                posts = jsonObj.getJSONArray("posts");
                int length = posts.length();
                for(int i=0; i< length; i++)
                {
                    MyProduct product = new MyProduct();
                    JSONObject p = posts.getJSONObject(i);

                    product.user_id = p.getInt("user_id");
                    product.title = p.getString("title");

                    /*Date parsed = new Date();
                    try {
                        SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
                        parsed = format.parse(p.getString("datetime"));
                    }
                    catch(ParseException pe) {
                        throw new IllegalArgumentException();
                    }*/

                    product.datetime = p.getString("datetime");
                    product.description = p.getString("description");
                    product.transaction = p.getString("transaction");
                    product.duration = p.getInt("duration");
                    product.foto1 = p.getString("foto1");
                    product.foto2 = p.getString("foto2");
                    product.foto3 = p.getString("foto3");
                    product.foto4 = p.getString("foto4");
                    product.foto5 = p.getString("foto5");
                    product.province = p.getString("province");
                    product.city = p.getString("city");
                    product.latitude = p.getDouble("latitude");
                    product.longitude = p.getDouble("longitude");
                    product.discount = p.getDouble("discount");
                    product.delivery = p.getDouble("delivery");
                    product.negotiable_price = p.getDouble("negotiable_price");
                    product.price = p.getDouble("price");
                    product.exchange = p.getDouble("exchange");
                    product.category = p.getString("category");
                    String visits = p.getString("visits");
                    product.visits = !visits.isEmpty()  ? Integer.parseInt(visits) : 0;
                    product.distance = p.getDouble("distance");

                    adapter.add(product);

                }



            } catch (JSONException e) {
                e.printStackTrace();
            }

            int lenght = adapter.getCount();
            adapter.notifyDataSetChanged();



        }

    }

}
