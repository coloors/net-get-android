package com.coloors.netget;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ngpeltzer on 3/2/16.
 */
public class Categories {
    private String name;
    private int res;

    public Categories(String name, int res)
    {
        this.name = name;
        this.res = res;
    }

    public void setIcon(int res) {
        this.res = res;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIcon() {
        return res;
    }

    public String getName() {
        return name;
    }


}
