package com.coloors.netget;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by ngpeltzer on 2/9/16.
 */
class MyProduct{
    public int user_id;
    public String title;
    public String datetime;
    public String description;
    public String transaction;
    public int duration;
    public String foto1;
    public String foto2;
    public String foto3;
    public String foto4;
    public String foto5;
    public String province;
    public String city;
    public Double latitude;
    public Double longitude;
    public Double discount;
    public Double delivery;
    public Double negotiable_price;
    public Double price;
    public Double exchange;
    public String category;
    public int visits;
    public Double distance;

    public MyProduct()
    {}

    public MyProduct(String title, String description) {
        this.title = title;
        this.description = description;


    }

}

public class ProductsListAdapter extends ArrayAdapter<MyProduct> {

    private ImageView imageView;

    public ProductsListAdapter(Context context, ArrayList<MyProduct> products) {
        super(context, 0, products);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        MyProduct product = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.products_grid_item, parent, false);
        }
        // Lookup view for data population
        TextView tvTitle = (TextView) convertView.findViewById(R.id.product_title);
        TextView tvCategorie = (TextView) convertView.findViewById(R.id.product_categorie);
        TextView tvPrice = (TextView) convertView.findViewById(R.id.product_price);
        TextView tvCity = (TextView) convertView.findViewById(R.id.product_city);
        imageView = (ImageView) convertView.findViewById(R.id.product_image);
        List<ImageView> list = new ArrayList<ImageView>();
        list.add(imageView);
        new DownloadImagesTask(list,new String("http://netandget.com/API/").concat(this.getItem(position).foto1)).execute();

        // Populate the data into the template view using the data object
        tvTitle.setText(product.title);
        tvCategorie.setText(product.category);
        tvPrice.setText(Double.toString(product.price));
        tvCity.setText(product.city);
        // Return the completed view to render on screen
        return convertView;
    }


}