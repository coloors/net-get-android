package com.coloors.netget;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

/**
 * Created by ngpeltzer on 2/13/16.
 */
public class DownloadImagesTask extends AsyncTask<Void, Void, Bitmap> {

    private List<ImageView> imgviews = null;
    private String url;

    public DownloadImagesTask(List<ImageView> imgviews, String url) {
        super();
        this.imgviews = imgviews;
        this.url = url;

    }




    @Override
    protected Bitmap doInBackground(Void... params) {
        return download_Image(this.url);
    }


    @Override
    protected void onPostExecute(Bitmap result) {
        for(int i=0; i<imgviews.size();i++) {
            if(imgviews.get(0) == null) return;
            Bitmap resized = Bitmap.createScaledBitmap(result, 256, 256, true);
            ImageView view = this.imgviews.get(i);
            view.setImageBitmap(resized);
        }

    }


    private Bitmap download_Image(String url) {
        Bitmap bitmap = null;
        try {
            URL url1 = new URL(url);
            bitmap = BitmapFactory.decodeStream(url1.openConnection().getInputStream());

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

}