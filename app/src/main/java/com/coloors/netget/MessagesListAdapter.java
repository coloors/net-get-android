package com.coloors.netget;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by ngpeltzer on 2/4/16.
 */

class MyMessage{
    public String name;
    public String description;

    public MyMessage(String name, String description) {
        this.name = name;
        this.description = description;
    }

}

public class MessagesListAdapter extends ArrayAdapter<MyMessage> {
    public MessagesListAdapter(Context context, ArrayList<MyMessage> messages) {
        super(context, 0, messages);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        MyMessage message = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.messages_list_item, parent, false);
        }
        // Lookup view for data population
        TextView tvName = (TextView) convertView.findViewById(R.id.messages_name);
        TextView tvHome = (TextView) convertView.findViewById(R.id.messages_description);
        // Populate the data into the template view using the data object
        tvName.setText(message.name);
        tvHome.setText(message.description);
        // Return the completed view to render on screen
        return convertView;
    }
}