package com.coloors.netget;

import android.app.Application;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ngpeltzer on 3/2/16.
 */
public class MyApp extends Application {
    private  List<Categories> categories;
    public List<Categories> getCategories()
    {
        if(categories != null) return categories;
        categories = new ArrayList<Categories>();
        Categories cat1 = new Categories("Empleos, Formación y negocios", R.drawable.cat_empleo);
        categories.add(cat1);
        Categories cat2 = new Categories("Casa y hogar", R.drawable.cat_casa);
        categories.add(cat2);
        Categories cat3 = new Categories("Compartir (Ayudas Sociales)", R.drawable.cat_compartir);
        categories.add(cat3);
        Categories cat4 = new Categories("Arte y cultura", R.drawable.cat_arte);
        categories.add(cat4);
        Categories cat5 = new Categories("Bebés y niños", R.drawable.cat_bebes);
        categories.add(cat5);
        Categories cat6 = new Categories("Vehículos", R.drawable.cat_vehiculos);
        categories.add(cat6);
        Categories cat7 = new Categories("Servicios", R.drawable.cat_servicios);
        categories.add(cat7);
        Categories cat8 = new Categories("Tecnología y videojuegos", R.drawable.cat_tecno);
        categories.add(cat8);
        Categories cat9 = new Categories("Mascotas y animales", R.drawable.cat_mascotas);
        categories.add(cat9);
        Categories cat10 = new Categories("Moda y accesorios", R.drawable.cat_moda);
        categories.add(cat10);
        Categories cat11 = new Categories("Belleza y salud", R.drawable.cat_belleza);
        categories.add(cat11);
        Categories cat12 = new Categories("Ocio y deportes", R.drawable.cat_ocio);
        categories.add(cat12);
        Categories cat13 = new Categories("Inmobiliaria", R.drawable.cat_inmo);
        categories.add(cat13);
        Categories cat14 = new Categories("Viajes y hoteles", R.drawable.cat_viajes);
        categories.add(cat14);
        Categories cat15 = new Categories("Gastronomía", R.drawable.cat_gastro);
        categories.add(cat15);
        Categories cat16 = new Categories("Cupones", R.drawable.cat_cupones);
        categories.add(cat16);
        Categories cat17 = new Categories("Todas las categorías", R.drawable.cat_todas);
        categories.add(cat17);



        return categories;
    }
}
